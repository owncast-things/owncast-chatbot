const emojiHelper = require("../emojis");
const storage = require("node-persist");
const config = require("config");
const jsdom = require("jsdom");
const {
    JSDOM
} = jsdom;

const owncast = require('../owncast');
const {
    resolveInclude
} = require("ejs");

const raceStatus = Object.freeze({
    "no_race": 1,
    "waiting": 2,
    "in_progress": 3,
    "finished": 4
});

// The delay to wait between the first player joining and the race starting.
// a.k.a. how mush time the other players have to join the race .
const raceWaitDelay = config.get('webhooks.race_waiting_delay');

const raceData = {
    status: raceStatus.no_race,
    players: [],
    durations: {
        fastest: {
            speed: 0,
            player: ''
        },
        slowest: {
            speed: 0,
            player: ''
        }
    }
};

const io = null;
storage.init({
    dir: 'persistence/rigsave'
});

// "!race 👵"
exports.raceCommand = async function(text, userId, userDisplayName) {
    console.log("Enabled features as seen from the emoji race: ", global.enabledFeatures);
    if (global.enabledFeatures.emojirace === true) {
        let rig_object = parseRaceCommand(text);
        if (rig_object === null) {
            // load saved emoji rig
            console.log(`Loading saved rig for ${userId}.`);
            rig_object = await loadRig(userId);
        }
        race(rig_object, userId, userDisplayName);
    } else {
        owncast.sendChatMessage("Oops! Race is disabled.");
    }
}

function getDefaultRig() {
    return {
        main: '🚗',
        hat: null,
        eyes: null,
        hands: null
    };
}

function race(rig, userId, userDisplayName) {
    console.log(`${userId} joins the race with this rig:`, rig);

    if (rig == null) {
        rig = getDefaultRig();
    }

    // Only allow users to start a new race or join an existing race during the waiting period.
    if (raceData.status === raceStatus.no_race || raceData.status === raceStatus.waiting) {
        // Get random stats for the player (e.g. speed)
        const player_properties = getRandomProperties()
        const playerData = {
            id: userId,
            display_name: userDisplayName,
            emojis: rig,
            properties: player_properties
        };

        if (FindPlayerInRace(userId) !== -1) {
            // If player was already in race, replace their previous settings.
            raceData.players[existingPlayerSlot] = playerData;
            owncast.sendChatMessage(`${userDisplayName} has changed their rig!`);
        } else {
            raceData.players.push(playerData);
            owncast.sendChatMessage(`${userDisplayName} has joined the race!`);
        }
        // If they're the first racer, start the waiting period to allow others to join.
        if (raceData.status === raceStatus.no_race) {
            setRaceStatus(raceStatus.waiting);
            owncast.sendChatMessage(`${userDisplayName} is challenging other viewers to an emoji race ! You have ${raceWaitDelay/1000} seconds to join!.`);
            global.io.sockets.emit('status_change', JSON.stringify({
                status: raceStatus.waiting,
                data: null
            }));
            console.log("waiting for other players...");
            setTimeout(startRace, raceWaitDelay);
        }
    } else {
        owncast.sendChatMessage("A race is already in progress! Please wait for a bit.");
    }
};


/**
 * Discover the first emoji in a string. An emoji is either a unicode Emoji, or an img tag.
 * #FIXME This function was written by a mad man.
 * @param {String} text 
 * @returns {string|null} Iether a string containing the unicode emoji, or the relative URL to the image in case the first emoji was an image.
 */


function getFirstEmoji(text) {
    // Main emoji
    // Extract all image tags from the body.
    const ImgUrls = emojiHelper.getImageEmojis(text);

    let firstImageEmoji = null;
    if (ImgUrls.length > 0) {
        const firstImageUrl = ImgUrls[0];
        if (isImageValid(firstImageUrl)) {
            firstImageEmoji = firstImageUrl;
        }
    }
    let firstImageEmojiPosition = text.indexOf("<img ");
    firstImageEmojiPosition = (firstImageEmojiPosition === -1) ? Number.MAX_SAFE_INTEGER : firstImageEmojiPosition;


    const unicodeEmojis = emojiHelper.getUnicodeEmojis(text);

    const firstUnicodeEmoji = (unicodeEmojis.length > 0) ? unicodeEmojis[0] : null;
    let firstUnicodeEmojiPosition = Number.MAX_SAFE_INTEGER;
    if (firstUnicodeEmoji !== null) {
        firstUnicodeEmojiPosition = text.indexOf(firstUnicodeEmoji);
    }
    let firstEmoji = (firstUnicodeEmojiPosition < firstImageEmojiPosition) ? firstUnicodeEmoji : firstImageEmoji;

    // If both positions are equal, it means they're MAX_SAFE_INTEGER
    // = no emoji was found at all.
    if (firstImageEmojiPosition === firstUnicodeEmojiPosition) {
        firstEmoji = null;
    }

    return firstEmoji;
}

exports.saveRig = async function(text, userId) {
    const rig = parseRaceCommand(text);
    if (rig !== null) {
        await storage.setItem(userId, rig);
    }
}

async function loadRig(user_id) {
    const rig = await storage.getItem(user_id);
    return rig;
}

function parseRaceCommand(text) {

    let rigObject = {
        main: null,
        hat: null,
        eyes: null,
        hands: null
    };

    const slotNamesAndPositions = new Map();
    slotNamesAndPositions.set('hat', Number.MAX_SAFE_INTEGER);
    slotNamesAndPositions.set('eyes', Number.MAX_SAFE_INTEGER);
    slotNamesAndPositions.set('hands', Number.MAX_SAFE_INTEGER);

    // As accessory slots can be specified in any order,
    // we need to figure out which one is first.
    let FirstSlotPosition = Math.min(
        slotNamesAndPositions.get('hat'),
        slotNamesAndPositions.get('eyes'),
        slotNamesAndPositions.get('hands'));
    if (FirstSlotPosition === Number.MAX_SAFE_INTEGER) {
        // No accessory slot found.
        FirstSlotPosition = text.length;
    }
    // The main emoji is between the beginning of the string and the first accessory.
    const mainEmoji = getFirstEmoji(text.substring(0, FirstSlotPosition));
    // If no main emoji was found, don't bother looking for accessories.
    if (mainEmoji !== null) {
        rigObject.main = mainEmoji;
        for (let [slotName, slotPosition] of slotNamesAndPositions) {
            let slot = slotName + ':';
            let pos = text.indexOf(slot);
            if (pos !== -1) {
                slotNamesAndPositions.set(slotName, pos);
                rigObject[slotName] = getFirstEmoji(text.substring(pos));
            }
        }
    } else {
        rigObject = null;
    }
    return rigObject;
}

function isImageValid(url) {
    // Does the image originate from our servers ?
    return url.startsWith("/");
}

/**
 * Check if a player is already part of the race.
 * @param {string} userId An owncast internal user id. 
 * @returns {int} Return the slot number for the player if they're already in the race. -1 if not.
 */
function FindPlayerInRace(userId) {
    // return -1 if player not found
    let index = -1;
    for (let i = 0; i < raceData.players.length; i++) {
        if (raceData.players[i].id === userId) {
            index = i;
            break;
        }
    }
    return index;
}

function getRandomNumber(min = Number.MIN_SAFE_INTEGER, max = Number.MAX_SAFE_INTEGER) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}

function getRandomProperties() {

    // Randomize speed!
    const duration = getRandomNumber(8, 15);

    // Randomize delay before the transition starts
    const delay = getRandomNumber(0, 500);

    // Randomize easing function
    const easingFunctionsChoices = ['ease', 'ease-in', 'ease-out', 'ease-in-out', 'linear'];
    const easing = easingFunctionsChoices[getRandomNumber(0, easingFunctionsChoices.length - 1)];

    return {
        duration: duration,
        easing: easing,
        delay: delay
    }
}

function startRace() {

    setRaceStatus(raceStatus.in_progress);
    let chatMessage = 'Race is about to start! ' + "\n";
    for (let i = 0; i < raceData.players.length; i++) {
        chatMessage += `Good luck ${raceData.players[i].display_name}! `;
    }
    owncast.sendChatMessage(chatMessage);

    // Find fastest and slowest times so we can send that info
    // to the client, which will use it to figure out who won.
    raceData.durations.fastest = findFastestSpeed(raceData);
    raceData.durations.slowest = findSlowestSpeed(raceData);

    global.io.sockets.emit('status_change', JSON.stringify(raceData));
    console.log("Race is starting with race data: ", raceData);
}

function findFastestSpeed(raceData) {
    let fastest = 1000;
    let playerName = '';
    for (let i = 0; i < raceData.players.length; i++) {
        if (raceData.players[i].properties.duration < fastest) {
            fastest = raceData.players[i].properties.duration;
            playerName = raceData.players[i].display_name;
        }
    }
    return {
        speed: fastest,
        player: playerName
    };
}

function findSlowestSpeed(raceData) {
    let slowest = 0;
    let playerName = '';
    for (let i = 0; i < raceData.players.length; i++) {
        if (raceData.players[i].properties.duration > slowest) {
            slowest = raceData.players[i].properties.duration;
            playerName = raceData.players[i].display_name;
        }
    }
    return {
        speed: slowest,
        player: playerName
    };
}

function setRaceStatus(status) {
    console.log("Changing race status to:", status);
    raceData.status = status;
}

global.io.on('connection', (socket) => {
    socket.on('status_change', (msg) => {
        console.log('Received a status change from client: ' + msg);
        global.io.sockets.emit('status_change', JSON.stringify({
            status: 1
        }));
        setTimeout(function() {
            setRaceStatus(raceStatus.no_race);
            raceData.players = [];
        }, 11000);
    });
});