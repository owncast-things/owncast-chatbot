const {
    get
} = require("express/lib/response");
const jsdom = require("jsdom");
const {
    JSDOM
} = jsdom;

exports.getAllEmojis = function(text) {
    // Extract Unicode emojis from the text.    
    const unicodeEmojis = getUnicodeEmojis(text);
    // Extract image emojis
    const imageEmojis = getImageEmojis(text);

    return unicodeEmojis.concat(imageEmojis);
}

function getUnicodeEmojis(text) {
    // #TODO Move the following calls to emojiHelper and replace with a call to a new emojiHelper.getEmojis() function.
    // Extract flag emojis. This has to be done separately since they're multi-character.
    const flagEmojis = getFlagEmojis(text);
    // Extract all emojis, except the flags, which we extracted above.
    const emojis = getUnicodeEmojisButFlags(text);

    return filterEmojis(emojis.concat(flagEmojis));
}
exports.getUnicodeEmojis = getUnicodeEmojis;

function getImageEmojis(text) {
    const messageFragment = JSDOM.fragment(text);
    // Extract all image tags from the body.
    const imgTags = messageFragment.querySelectorAll("img");

    const validImages = [];
    [].forEach.call(imgTags, function(imgTag) {
        const imageUrl = imgTag.src;
        if (isImageValid(imageUrl)) {
            // If valid, add the url to the wall source
            validImages.push(imageUrl);
        }
    });

    return validImages;
}
exports.getImageEmojis = getImageEmojis;

function getFlagEmojis(text) {
    // Extract flag emojis
    // Each flag emoji is extracted as two emojis (https://en.wikipedia.org/wiki/Regional_indicator_symbol)
    const flagEmojisParts = text.match(/[\u{1f1e6}-\u{1f1ff}]/gu);

    // We need to concat them two by two for the emoji wall to display them as flags.
    let flagEmojisjoined = [];
    if (flagEmojisParts !== null) {
        // Group them 2 by 2 !!
        for (let i = 0; i < flagEmojisParts.length - 1; i += 2) {
            let flagEmoji = flagEmojisParts[i] + flagEmojisParts[i + 1];
            flagEmojisjoined.push(flagEmoji);
        }
    }

    return flagEmojisjoined;
}

function getUnicodeEmojisButFlags(text) {
    // The following block aims at getting all other emojis (i.e. all emojis, minus the flags).
    // TODO : Replace all of it with one single regexp: "Emojis excluding the flags range".
    // Extract all emojis
    const unicodeEmojis = text.match(/\p{Emoji}/gu);
    // This variable will contain all emojis minus the flag emojis
    let filteredEmojis = [];
    // Filter out the flag emojis
    if (unicodeEmojis !== null) {
        [].forEach.call(unicodeEmojis, function(emoji) {
            if (emoji.match(/[\u{1f1e6}-\u{1f1ff}]/gu) === null) {
                filteredEmojis.push(emoji);
            }
        });
    }
    return filteredEmojis;
}

function filterEmojis(emojis, unwantedEmojis = null) {
    // TODO regex match!
    // Numbers, asterisk and hash are emojis too (really!), but we don't want them :'(
    if (unwantedEmojis === null) {
        unwantedEmojis = ['*', '#', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
    }
    // Remove unwanted emojis from our extracted emojis
    return emojis.filter(x => unwantedEmojis.indexOf(x) === -1);
}

function isImageValid(url) {
    // Does the image originate from our servers ?
    return url.startsWith("/");
}