const express = require("express");
const app = express();
const http = require("http");
const server = http.createServer(app);
const {
    Server
} = require("socket.io");
global.io = new Server(server);
const bodyParser = require("body-parser");
const config = require("config");
const chatbot = require("./src/chatbot")
const emojiRace = require("./src/emoji-race");

/**
 * Keep track of features enabled through moderator chatbot commands.
 * Startup state is set through the config file.
 */
global.enabledFeatures = {
    tts: config.get("webhooks.enabled_features.tts"),
    emojirace: config.get("webhooks.enabled_features.emojirace")
};

app.set("view engine", "ejs");
app.use(express.static("public"));
app.use(
    bodyParser.urlencoded({
        extended: false,
    })
);
app.use(bodyParser.json());

/* Routes. */
app.get("/", (req, res) => {
    res.render("index", {
	    sockets_prefix: config.get('webhooks.prefix')
    });
});
app.get("/emoji-race", (req, res) => {
    res.render('emoji-race', {
        sockets_host: config.get('webhooks.host'),
        sockets_prefix: config.get('webhooks.prefix'),
        images_host: config.get('owncast.host')
    });
});
app.get('/tts', (req, res) => {
    res.render('tts', {
        sockets_host: config.get('webhooks.host'),
        sockets_prefix: config.get('webhooks.prefix')
    });
});

// Route to be called by owncast for 'chat send' events.
app.post("/hooks/chat_send/:password", function(req, res) {
    console.log("Incoming webhook! /hooks/chat_send")
    
    if (check_password(req.params.password)) {
        handleChatMessage(req.body);
        res.json({
            status: 200,
        });
    } else {
        res.json({ status: 403 });
    }
});

function check_password(password) {
    return config.get('password') === password;
}

/**
 * Find chatbot commands and call the appropriate code.
 * If no chatbot command is found, extract emojis and send them to the emoji wall.
 * @param {String} message Owncast webhook event as decribed at https://owncast.online/thirdparty/webhooks/#chat
 */
function handleChatMessage(message) {
    console.log("Received a message:", message);
    command = chatbot.findChatbotCommand(message.eventData.body);
    if (command !== null) {
        console.log(`Found a chatbot command: "${command}"`);

        // The internal user ID used by Owncast
        const userId = message.eventData.user.id;
        const userDisplayName = message.eventData.user.displayName;

        // Remove the command from the message.
        // For example, so that the TTS doesn't say "!say hello" but only "hello".
        var text = message.eventData.body.replace(command, "").trim();

        switch (command) {
            case "!bot":
                chatbot.bot();
                break;
            case "!details":
                chatbot.details();
                break;
            case "!dis":
            case "!say":
                chatbot.say(command, text)
                    .catch(error => console.log("There was error executing the chatbot 'say'/'dis' command:", error));
                break;
            case "!race":
                emojiRace.raceCommand(text, userId, userDisplayName)
                    .catch(error => console.log("There was error executing the chatbot 'race' command:", error));
                break;
            case "!saverig":
                emojiRace.saveRig(text, userId)
                    .catch(error => console.log("There was error executing the chatbot 'saverig' command:", error));
                break;
            case "!mod":
                chatbot.mod(text, userId);
                break;
        }
    }
}

server.listen(config.get("webhooks.port"), () => {
    console.log("listening on *:" + config.get("webhooks.port"));
});

console.log("Enabled features: ", global.enabledFeatures);
