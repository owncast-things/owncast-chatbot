
# Chatbot for Owncast

A chatbot for [Owncast](https://owncast.online/).

Because the chatbot makes use of the Owncast webhooks, it needs to be hosted on a system reachable by your owncast server. The easiest way is to run both on the same server!

- [Features](#features)
  - [Chatbot User commands](#chatbot-user-commands)
  - [Emoji race commands](#emoji-race-commands)
  - [Moderators-only commands](#moderators-only-commands)
- [Installation](#installation)
- [Configuration](#configuration)
  - [Chatbot config file](#chatbot-config-file)
  - [Owncast webhooks and tokens](#owncast-webhooks-and-tokens)
  - [OBS overlays](#obs-overlays)
- [What is that "prefix" thing?](#what-is-that-prefix-thing)
- [Contact](#contact)

## Features

- **Emoji wall**: emojis typed in chat will fly across the screen.
- **Text to speech** in French and English, using the watson tts api (it has a free tier). The spoken text is also displayed on screen.
- **Emoji race**: let viewers set up races between their favorite emojis on screen!
- **Moderators only commands**: let Owncast chat moderators turn on or off some features.

### Chatbot User commands

- `!bot` : displays a list of commands and a brief description.
- `!say <something to say>` : Text to speech in English.
- `!dis <something in French to say>` : Text to speech in French.
- `!race` : see below.
- `!saverig` : see below.

### Emoji race commands

The `!race` command is used to start or join a race.

If no parameter is supplied, it will try to load any emoji you previously saved. If no saved emoji is found, a default emoji is used.

You can **chose an emoji** by joining the race using a command such as `!race 🐎`.

Any emoji found in the emoji picker from the Owncast chat can be used. Any Unicode emoji can also be used, regardless if it is available in the Owncast emoji picker.

You can **accessorize your emoji** by specifying one or more of the following options :
- `hat` : the emoji will be placed on top of the main one, just like a hat!
- `eyes` : the emoji will be placed in front of the main one, at the approximate location where eyes would be.
- `hands` : the emoji will be placed at the bottom right corner of the main emoji. It might look like the main emoji is holding it, hence the name.

Examples: 

- `!race 👴 hat:🎩`
- `!race 😋 eyes:🕶 hands:🍺`

Bear in mind that every emoji is different, so the placement and alignment my not be perfect for all combinations!

If you don't want to have to type it all every time you join a race, you can **save your settings** by using the `!saverig` command :
- `!saverig 👽 hands:🏆 hat:🌸 eyes:👀`

The next time you join the race using `!race` without any other parameter, your saved emojis will be used.

Note that we use the user id assigned to you by Owncast to locate your saved emojis. It means that if your user id changes (for example you lost your cookie!), we won't be able to load your saved emojis!

### Moderators-only commands

The following commands are only available to users who have been assigned the role of moderators in the Owncast chat.

- `!mod list` : displays a list of the mod commands and a brief description.
- `!mod tts on` and `!mod tts off` : enables or disables the text to speech feature.
- `!mod emojiwall on` and `!mod emojiwall off` : enables or disables the emoji wall.
- `!mod emojirace on` and `!mod emojirace off` : enables or disables the emoji race.

Note: if you plan on not using one of the features, you can also turn it off from the config file.

## Installation

1. Make sure your server has NodeJS, NPM and git installed;
2. Clone this repo on your server;
3. Execute `./chatbot install`
4. [Edit the config file](#chatbot-config-file) according to your usage.

To start the chatbot, use:

```
sudo systemctl start owncast-chatbot.service
```

## Configuration

### Chatbot config file

- `watson` : fill this in if you want to use the TTS feature. *(To be completed)*
- `owncast`: this section of the config file is the most important, as it will enable the chatbot to interact with your Owncast server.
  - `owncast > tokens > chat > send`: An Owncast access token with the following permission: *"Can send chat messages on behalf of the owner of this token."*. You can get it from the admin area of your Owncast server at `/admin/access-tokens`.
  - `owncast > tokens > chat > system`: Not used at the moment.
  - `owncast > stream_key`: your Owncast stream key.
  - `owncast > host`: the FQDN of your Owncast server.
- `webhooks`
  - `webhooks > host`: The FQDN of the server where the chatbot runs. Usually the same as the Owncast server.
  - `webhooks > prefix`: Please see [this explanation](#what-is-that-prefix-thing).
  - `webhooks > tts_audio_files_path`: The TTS feature saves audio files to disk. This is where they will be saved. You can probably leave it at the default value, but if you change it make sure the location is also served by your web server.
  - `webhooks > race_waiting_delay`: How much time the other players have to join the race after the first player initiated it.
  - `webhooks > port`: The port on which the chatbot server (nodejs) will listen.
  - `enabled_features`: Here you can disable some of the features of the chatbot. Moderators will still be able to enable them.

### Owncast webhooks and tokens

There are two things you need to do on your Owncast instance:

1. [Set up a webhook call](https://owncast.online/thirdparty/webhooks/) to the `/hooks/chat_send` URL of your chatbot server for the CHAT event (*"When a user sends a chat message"*).
If you have configured "/chatbot" as a [location prefix](#what-is-that-prefix-thing), the URL might look like `https://stream.example.com/chatbot/hooks/chat_send`
2. [Create a standard chat access token ](https://owncast.online/thirdparty/apis/) to fill in the setting `owncast > tokens > chat > send` in the [chatbot config file](#chatbot-config-file).

### OBS overlays

To display the emoji wall, emoji race or TTS on your stream, you can set up sources in OBS which capture browser windows, or directly use "browser sources" (the latter is recommanded).

If you use browser sources, make sure to check "Control audio via OBS" in the source properties for the TTS and emoji race, or you won't be able to hear the sound on stream. The emoji wall does not have sound.

The pages to show are :
- `/emoji-wall`
- `/emoji-race`
- `/tts`
 
Don't forget about [the prefix](#what-is-that-prefix-thing) you chose if you run the chatbot on the same server as Owncast. If your prefix is `/chatbot`, the URLs might look like :
- `https://stream.example.com/chatbot/emoji-wall`
- `https://stream.example.com/chatbot/emoji-race`
- `https://stream.example.com/chatbot/tts`

## What is that "prefix" thing?

Since you probably want to run the chatbot on the same server as Owncast, you need to configure your reverse proxy to serve the chatbot under a different location.

You also need to let the chatbot know what its URL is.

For example, my owncast server is at `https://direct.disquette.top`, and the chatbot lives at `https://direct.disquette.top/chabot`.

Here's an excerpt from my nginx config showing the two different locations ;

```nginx
location / {
  (...)
  proxy_pass http://127.0.0.1:8080;
}

location /chatbot {
  rewrite /chatbot/?(.*) /$1  break;
  (...)
  proxy_pass http://127.0.0.1:5000;
}
```

The '/chatbot' prefix and the port number must be specified in the chatbot config file in the `webhooks > prefix` and `webhooks > port` settings, respectively.


## Contact

Feel free to reach out on the fediverse at [@fractal@cybre.space](https://cybre.space/@fractal) or open an issue in this repository.



